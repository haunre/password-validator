package Sheridan.PasswordValidator;

import java.util.Scanner;
import java.util.regex.Pattern;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author 123
 */
public class PasswordValidator {
    private static final String REGDIGIT = "^(?=.*\\d)$";
    private static final String REGCHAR = "^(?=.[@$+!?^&])";
    private static final String REGCAPITAL = "^(?=.*[A-Z])";
    private static final String REGMINLENGTH = "^.[8]";
    
    private static final Pattern PATTERN1 = Pattern.compile(REGDIGIT);
    private static final Pattern PATTERN2 = Pattern.compile(REGCHAR);
    private static final Pattern PATTERN3 = Pattern.compile(REGCAPITAL);
    private static final Pattern PATTERN4 = Pattern.compile(REGMINLENGTH);

//Methods
    private static boolean checkDigit(String password) {
        return PATTERN1.matcher(password).matches();
    }
    
    private static boolean checkSpecialCharacter(String password) {
        return PATTERN1.matcher(password).matches();
    }
    private static boolean checkCapitalLetter(String password) {
        return PATTERN1.matcher(password).matches();
    }
    private static boolean checkMinLength(String password) {
        return PATTERN1.matcher(password).matches();
    }
    private static boolean isValidPassword(String userInput) {
        return checkDigit(userInput) &&
                checkSpecialCharacter(userInput) &&
                checkCapitalLetter(userInput) &&
                checkMinLength(userInput); // stops as soon as it find a false after the check
    }
    
    public static void main(String[]args){
        String password = "Helloworld";
        
        System.out.println(PasswordValidator.isValidPassword(password));
        
        String badpassword = "hello";
        
        System.out.println(PasswordValidator.isValidPassword(badpassword));
    }
}
